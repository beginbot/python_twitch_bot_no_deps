# Simple Python Twitch Chat Bot with No Dependencies

Get Token [here](https://twitchapps.com/tmi/)

Note: Make sure you don't EVER SHARE OR COMMIT YOUR TOKEN.

## How to Run

```bash
export TWITCH_OAUTH_TOKEN="OAUTH_TOKEN_FROM_LINK_DO_NOT_SHARE"
export BOT_NAME="INSERT_BOT_NAME"
export CHANNEL="beginbot"

git clone git@gitlab.com:beginbot/python_twitch_bot_no_deps.git

cd python_twitch_bot_no_deps

python bot.py

# Or on one line
BOT_NAME="INSERT_BOT_NAME" CHANNEL="beginbot" python bot.py
```
